import { useContext } from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { userContext } from "./context/userContext";
import Layout from "./components/Layout/Layout";
import Header from "./components/Header/Header";
import RestrictedRoute from "./components/RestrictedRoute/RestrictedRoute";
import PrivateRoute from "./components/PrivateRoute/PrivateRoute";
import LoginView from "./views/loginView/LoginView";
import RegisterView from "./views/RegisterView/RegisterView";
import ChatBlockView from "./views/ChatBlockView/ChatBlockView";
import ProfileView from "./views/ProfileView/ProfileView";

function App() {
  const { loading } = useContext(userContext);
  return (
    <>
      <Header />
      <Layout>
        {loading ? (
          <h1 className="loading">Loading...</h1>
        ) : (
          <Switch>
            <RestrictedRoute exact path="/login" redirectTo="/">
              <LoginView />
            </RestrictedRoute>
            <RestrictedRoute exact path="/register" redirectTo="/">
              <RegisterView />
            </RestrictedRoute>

            <PrivateRoute path="/profile" redirectTo="/login" exact>
              <ProfileView />
            </PrivateRoute>

            <PrivateRoute path="/" redirectTo="/login" exact>
              <ChatBlockView />
            </PrivateRoute>

            <Route>
              <Redirect to="/login" />
            </Route>
          </Switch>
        )}
      </Layout>

      <ToastContainer position="top-center" autoClose={3000} />
    </>
  );
}
export default App;
